const router = require('express').Router();

router.post('/exam01', (req, res)=>{
    var data = req.body;
    var result =[]
    
    //알고리즘 처리
    data.map(data=>{
        if(data.input.length < 1 || data.input.length > 10){
            result.push({
                "input" : data.input,
                "output" : "length error"
            });
        }else if(data.input.charAt(0) == 0){
            result.push({
                "input" : data.input,
                "output" : "첫번째 index에 0이 포함됨"
            });
        }else if(!data.input.match(/\d/g)){
            result.push({
                "input" : data.input,
                "output" : "Not a Number"
            });
        }else{
            result.push({
                "input" : data.input,
                "output" : data.input.replace(/[^0-9]/g,"")
            });
        }
    })

    res.json(result);
})

router.post('/exam02', (req, res)=>{
    var data = req.body;
    var result = data.map(data=>{
        var breakPoint = Math.floor( data.input[0] / (data.input[2] - data.input[1] )) + 1;
        if(breakPoint > 1){
            return({
                "input" : data.input,
                "output": breakPoint
            });
        }else{
            return({
                "input" : data.input,
                "output": "-1"
            });
        }
    })
    res.send({"result": result})
})

router.post('/exam03', (req, res)=>{
    var data = req.body;
    var output = [];
    data.map(data=>{
        var input = data.input;
        var result = "";
        while(input>0){
            if(input%3 === 0){
                result = '6' + result;
                input = input/3 -1;
            }else if(input%3 === 1){
                result = '2' + result;
                input = Math.floor(input/3);
            }else{
                result = '4' + result;
                input = Math.floor(input/3);
            }
        }
        output.push({
            "input" : data.input,
            "output": result
        });
    })
    res.send(output)

})

router.post('/exam04', (req, res)=>{
    var result = req.body;
    result.output = [];
    let hanoi = (n, from, to)=>{    //n: 원반의 수, from: 시작점, to: 도착점 
        var via = 6-from-to;
        if(n===1){
            result.output.push([`${from}, ${to}`]);
            console.log(`${n}을 ${from}에서 ${to}로 이동`);
        }else{
            hanoi(n-1, from, via);  // 마지막 원반을 옮기기 위해 위에 있는 원반들 경유지로 이동
            result.output.push([`${from}, ${to}`]);
            console.log(`${n}을 ${from}에서 ${to}로 이동`);
            hanoi(n-1, via, to);  // 경유지에 있는 원반을 목적지로 이동
        }
    }

    hanoi(result.input, 1, 3);
    res.send(result)
})


router.post('/exam05', (req, res)=>{
    var input = req.body.input;
    var temp;
    for(var currentIdx=0; currentIdx<input.length; currentIdx++){
        var min_idx = currentIdx;
        for(var compareIdx=currentIdx+1; compareIdx < input.length; compareIdx++){
            if(input[min_idx] >= input[compareIdx]){
                min_idx = compareIdx;
            }
        }
        temp = input[currentIdx];
        input[currentIdx] = input[min_idx];
        input[min_idx] = temp;
    }
    res.send({"result" : input})
})

router.post('/exam06', (req, res)=>{
    var input = req.body.input;
    for(var currentIdx=1; currentIdx<input.length; currentIdx++){
        var key = input[currentIdx];
        for(var compareIdx = currentIdx-1; compareIdx>=0; compareIdx--){
            if(input[compareIdx] > key){
                input[compareIdx + 1] = input[compareIdx];
                input[compareIdx] = key;
            }else{
                break;
            }
        }
    }
    res.send({"result": input})
})

router.post('/exam07', (req, res)=>{
    var input = req.body.input;
    for(var idx = 0; idx < input.length -1; idx++){
        for(var compareIdx = 0; compareIdx < input.length-1; compareIdx++){
            if(input[compareIdx] > input[compareIdx+1]){
                var temp = input[compareIdx];
                input[compareIdx] = input[compareIdx+1];
                input[compareIdx+1] = temp;
            }
        }
    }

    res.send({ "result" : input })
})


router.post('/exam08/NIP', (req, res)=>{
    var input = req.body.input;
    var output = [];

    function solution(input) {
        var leftArr = [];
        var rightArr = [];
        var pivot = input[0];
        
        for(var idx = 1; idx < input.length; idx++){
            pivot >= input[idx] ? leftArr = leftArr.concat(input[idx]) : rightArr = rightArr.concat(input[idx]);
        }
        if(leftArr.length>1){ leftArr = solution(leftArr) };
        if(rightArr.length>1){ rightArr =  solution(rightArr) };

        return leftArr.concat(pivot, rightArr);
    }

    output = solution(input);
    res.send({ "result": output });
})

// router.post('/exam08/IP', (req, res)=>{
//     var input = req.body.input;
//     var output = [];

//     function solution(input) {
//         var leftArr = [];
//         var rightArr = [];
//         var pivot = input[Math.floor(input.length / 2)];

//         for(var idx = 0; idx < input.length; idx++){
            
//             if(pivot >= input[idx]){
//                 leftArr = leftArr.concat(input[idx]);
//             }
//             if(pivot < input[idx]){
//                 rightArr = rightArr.concat(input[idx]);
//             }
//         }
        
//         if(leftArr.length === 2 && leftArr[1] === pivot){ return leftArr.concat(rightArr) }
//         if(rightArr.length === 2 && rightArr[1] === pivot){ return leftArr.concat(rightArr) }

//         if(leftArr.length>1){ leftArr = solution(leftArr) };
//         if(rightArr.length>1){ rightArr =  solution(rightArr) };

//         return leftArr.concat(rightArr);
        
//     }

//     output = solution(input);
//     res.send(output);
// })

router.post('/exam09', (req, res)=>{
    var arr = [];
    var input = req.body.input;
    var cnt = 0;
    for(var idx = 1; idx <= 100000; idx++){
        arr.push(idx);
    }

    // function solution(binarySearchArr){
    //     var centerIdx = Math.floor(binarySearchArr.length / 2);
    //     var center = binarySearchArr[centerIdx];
    //     cnt++;
    //     if(center < input){
    //         binarySearchArr = binarySearchArr.slice(centerIdx+1);
    //     }else if(center > input){
    //         binarySearchArr = binarySearchArr.slice(0, centerIdx);
    //     }else{
    //         return `탐색 횟수 : ${cnt}회, [${centerIdx}] 위치에서 ${input}을 찾았습니다.`;
    //     }
    //     var output = solution(binarySearchArr);
    //     return output;
    // }

    function solution(binarySearchArr){
        var leftIdx = 0;
        var rightIdx = binarySearchArr.length - 1;
        
        while(leftIdx <= rightIdx){
            cnt++;
            var centerIdx = Math.floor((leftIdx + rightIdx) / 2);
            var center = binarySearchArr[centerIdx];
            if(center < input){
                leftIdx = centerIdx + 1;
            }else if(center > input){
                rightIdx = centerIdx;
            }else{
                return `탐색 횟수 : ${cnt}회, [${centerIdx}] 위치에서 ${input}을 찾았습니다.`;
            }
        }
        return `탐색 횟수 : ${cnt}회, 해당 값을 찾지 못했습니다.`;
    }

    res.send({"result": solution(arr)})
})

router.post('/exam10', (req, res)=>{
    var body = req.body;
    var m = body.m;
    var tree = body.tree;
    var maxHeight = 0;
    for(var idx in tree){
        if(maxHeight <= tree[idx]){
            maxHeight = tree[idx];
        }
    }
    var start = 0;
    var end = maxHeight;
    while(start <= end){
        var sum = 0;
        var center = Math.floor((start + end) / 2);
        for(var i in tree){
            if(center < tree[i]){ sum += tree[i] - center }
        }
        if(sum < m){
            end = center;
        }else if( sum > m){
            start = center + 1;
        }else{
            break;
        }
    }
    body.output = center;
    res.json(body)
})


module.exports = router;