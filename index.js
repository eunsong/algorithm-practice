
const express = require('express');
const app = express();
const bodyParser = require('body-parser');


const apiRouter = require('./routes/api.route');

app.use(bodyParser.json());
app.use('/', apiRouter);


const PORT = process.env.PORT || 3000;
app.listen(PORT, ()=>{console.log(`server is running in ${PORT}!`)});